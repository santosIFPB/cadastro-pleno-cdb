let times = [];
let jogos = [];

function addTime() {
  let nome = document.getElementById("nomeTime").value;
  times.push(nome);
  placares();
}

function montarOpcoesTimes(id) {
  let opcoes = `<select id="${id}">`;
  for(let i of times) {
    opcoes += "<option>"+i+"</option>";
  }
  opcoes += "</select>";
  return opcoes;
}

function componenteJogo() {
  // select com times + input + X + input + times
  let jogo = montarOpcoesTimes("nm");
  const inputM = '<input type="text" id="gm">';
  const inputV = '<input type="text" id="gv">';
  jogo += inputM + " X " + inputV;
  jogo += montarOpcoesTimes("nv");
  return jogo;
}

function placares() {
  const comJogo = componenteJogo();
  let placar = "";
    placar += comJogo + "<br>";
  let div = document.getElementById("placar");
  div.innerHTML = placar;
}
placares()

  function cadastrarPlacar() {
    let mandante = document.getElementById("nm").value;
    let visitante = document.getElementById("nv").value;
    let placarMandante = document.getElementById("gm").value;
    let placarVisitante = document.getElementById("gv").value;
    jogos.push({visitante, placarMandante, placarVisitante, mandante})
    
    let numJogos = jogos.length;
    let tabelaDePlacares = document.getElementById("tabelaDePlacares");
    let lista = "<ol>";
  for (let {visitante, placarMandante, placarVisitante, mandante} of jogos) {
    lista = '<li>' + mandante + ' ' + placarMandante + ' X ' + placarVisitante + ' ' + visitante + '</li>' + '<br>';
  }
  lista += "</ol>";
  tabelaDePlacares.innerHTML = lista;
}
cadastrarPlacar();



